package currencyconverter.moad.jh.currencyconverter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by j on 02.12.16.
 */

public class EditCurrencyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_currency);


        final String currencyName = (String) getIntent().getSerializableExtra("currencyName");
        double rateForOneEuro = (double) getIntent().getSerializableExtra("rateForOneEuro");
        Log.i("EditCurrencyActivity", "currencyName:" + currencyName + "\tRate for one eur: " + rateForOneEuro);

        TextView textView = (TextView) findViewById(R.id.edit_textview);
        textView.setText("Edit rate of " + currencyName + " - ");

        final EditText editText = (EditText)findViewById(R.id.edit_edittext);
        editText.setText(String.valueOf(rateForOneEuro));
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("rateForOneEuro", Double.parseDouble(editText.getText().toString()));
                    Log.i("EditText", "new Currency = " + editText.getText().toString());
                    setResult(RESULT_OK, returnIntent); //send Result data after closing current Activity
                    finish(); // Close on Return pressed EditText
                    return true;
                }
                return false;
            }
        });

    }
}
