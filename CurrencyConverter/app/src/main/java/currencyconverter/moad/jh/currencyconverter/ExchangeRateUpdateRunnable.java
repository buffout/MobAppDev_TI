package currencyconverter.moad.jh.currencyconverter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by j on 12.01.17.
 */

public class ExchangeRateUpdateRunnable implements Runnable {

    private Context context; //field for main activity (casted from main context)
    private ExchangeRateDatabase database;

    private ExchangeRateUpdateNotifier exchangeRateUpdateNotifier;


    public ExchangeRateUpdateRunnable(ExchangeRateDatabase exchangeRateDatabase, Context context) {
        this.database = exchangeRateDatabase;
        this.context = context;  //keep main activity to gain access to UI thread to show Toast message

        this.exchangeRateUpdateNotifier = new ExchangeRateUpdateNotifier(this.context);
    }

    @Override
    public void run() {
        final boolean success = updateCurrencies(); // update Currency Rate in this Thread

        final String message = "Exchange Rate - " + (success ? "successful" : "failed");
        exchangeRateUpdateNotifier.showOrUpdateNotification(message);

        if (context instanceof Activity){
            final Activity activity = (Activity) context;
            //Start another new thread that will be passed to the UI to show Information that ist only available in this class.
            activity.runOnUiThread(new Runnable() { //Pass new Thread to provided main Activity
                @Override
                public void run() {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    private boolean updateCurrencies() {
        String queryString = String.format("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");

        try {
            URL url = new URL(queryString);

            URLConnection connection = url.openConnection();

            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(connection.getInputStream(), connection.getContentEncoding());

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if ("Cube".equals(parser.getName()) && parser.getAttributeCount() == 2) { //Rates are in Cube tags with more than one attrigbutes
                        String currency = parser.getAttributeValue(null, "currency");
                        double rate = Double.valueOf(parser.getAttributeValue(null, "rate"));
                        this.database.setExchangerate(currency, rate);
                    }
                }
                eventType = parser.next(); //nextTag() will throw an exception due to TEXT in previous gesmes:subject TAG
            }
            this.database.saveExchangeRates(context);
            return true;
        } catch (Exception e) {
            Log.e("RefreshRates", "Cannot query Exchangerate data.");
            e.printStackTrace();
            return false;
        }
    }
}
