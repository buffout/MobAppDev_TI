package currencyconverter.moad.jh.currencyconverter;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.util.Log;


/**
 * Created by j on 20.01.17.
 */
public class ExchangeRateUpdateJobService extends JobService {

    private ExchangeRateDatabase database;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Context context = this; //Android Services = Context - Required by database to access filesystem
        database = new ExchangeRateDatabase();
        Thread refreshCurrencyThread = new Thread(new ExchangeRateUpdateRunnable(database, context));
        refreshCurrencyThread.start();
        Log.d("ExchangeRateJobService", "Started");
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.d("ExchangeRateJobService", "Stopped");
        return false; // @return = rescheduling required
    }
}
