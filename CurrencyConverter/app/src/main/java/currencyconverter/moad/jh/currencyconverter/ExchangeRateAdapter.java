package currencyconverter.moad.jh.currencyconverter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by j on 10.11.16.
 */

public class ExchangeRateAdapter extends BaseAdapter{

    private ExchangeRateDatabase database;

    public ExchangeRateAdapter(ExchangeRateDatabase exchangeRateDatabase){
        this.database = exchangeRateDatabase;
    }

    @Override
    public int getCount() {
        return this.database.getCurrencies().length;
    }

    @Override
    public ExchangeRate getItem(int i) {
        String name = this.database.getCurrencies()[i];
        String capital = this.database.getCapital(name);
        double rate = this.database.getExchangeRate(name);
        return new ExchangeRate(name, capital, rate);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        ExchangeRate exchangeRate = this.getItem(i);

        if(view == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.content_currency_list, null, false);
        }

        String drawableName = "flag_" + exchangeRate.getCurrencyName().toLowerCase();
        int resId = context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName());

        ImageView imageView = (ImageView)view.findViewById(R.id.content_currency_list_ImageView);
        //imageView.setBackground(R.drawable.);
        imageView.setImageResource(resId);

        TextView currencyNameTextView= (TextView)view.findViewById(R.id.content_currency_listTextView_CurrencyName);
        currencyNameTextView.setText(exchangeRate.getCurrencyName());

        TextView exchangeRateTextView = (TextView)view.findViewById(R.id.content_currency_listTextView_ExchangeRate);
        exchangeRateTextView.setText(String.valueOf(exchangeRate.getRateForOneEuro()));

        return view;
    }
}
