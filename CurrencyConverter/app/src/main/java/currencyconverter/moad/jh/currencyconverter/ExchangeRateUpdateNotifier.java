package currencyconverter.moad.jh.currencyconverter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * Created by j on 17.01.17.
 */

public class ExchangeRateUpdateNotifier {
    private static final int NOTIFICATION_ID= 1337;

    NotificationCompat.Builder notificationBuilder;
    NotificationManager notificationManager;

    public ExchangeRateUpdateNotifier(Context context) {
        notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Refreshing Exchange Rates...")
                .setAutoCancel(false);

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, 0);
        notificationBuilder.setContentIntent(resultPendingIntent);

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void showOrUpdateNotification(String text){
        notificationBuilder.setContentText(text);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }


    public void removeNotification(){
        notificationManager.cancel(NOTIFICATION_ID);
    }
}
