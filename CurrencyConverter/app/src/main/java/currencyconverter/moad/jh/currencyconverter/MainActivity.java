package currencyconverter.moad.jh.currencyconverter;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {

    private Spinner toSpinner;
    private Spinner fromSpinner;
    private TextView resultTextView;
    private EditText inputEditText;
    private ShareActionProvider shareActionProvider;
    private ExchangeRateDatabase exchangeRateDatabase;
    private ExchangeRateAdapter exchangeRateAdapter;
    private ExchangeRateUpdateRunnable exchangeRateUpdateRunnable;
    private JobScheduler scheduler;
    private JobInfo jobInfo;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        fromSpinner = (Spinner) findViewById(R.id.Settings_From_Spinner_Currency);
        toSpinner = (Spinner) findViewById(R.id.Settings_To_Spinner_Currency);
        inputEditText = (EditText) findViewById(R.id.Input_EditText);
        resultTextView = (TextView) findViewById(R.id.Result_TextView);

        // Currency Data Adapter
        /*
        exchangeDatabase = new ExchangeRateDatabase();
        ArrayAdapter<String> fromAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, exchangeDatabase.getCurrencies());
        fromAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        */
        this.exchangeRateDatabase = new ExchangeRateDatabase();
        exchangeRateDatabase.loadExchangeRates(this);
        this.exchangeRateAdapter = new ExchangeRateAdapter(exchangeRateDatabase);
        this.exchangeRateUpdateRunnable = new ExchangeRateUpdateRunnable(exchangeRateDatabase, this);

        this.fromSpinner.setAdapter(exchangeRateAdapter);
        this.toSpinner.setAdapter(exchangeRateAdapter);

        //Exercise 5 - refresh currency rates from the web
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);

        //Exercise 9 - JobService
        ComponentName serviceName = new ComponentName(this, ExchangeRateUpdateJobService.class);
        jobInfo = new JobInfo.Builder(1337, serviceName) //IMPORTANT: requires permission - BIND_JOB_SERVICE
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setRequiresDeviceIdle(false) //  true = run service with low CPU utilization
                .setPeriodic(1000) // start background service after 10 seconds
                //.setPersisted(true) // Service will survive reboot
                .build();
        scheduler = (JobScheduler)getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.currency_list_menu, menu);

        MenuItem shareItem = menu.findItem(R.id.action_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

        MenuItem enableUpdateService = menu.findItem(R.id.action_enable_update_service);

        // reset menu item preferences
        pref = getPreferences(Context.MODE_PRIVATE);
        enableUpdateService.setChecked(pref.getBoolean("enableServiceChecked", false));

        setShareText(null);
        return true;
    }

    private void setShareText(String text){
        Intent shareIntent= new Intent(Intent.ACTION_SEND);

        shareIntent.setType("text/plain");
        if(text != null){
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        }

        shareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        pref = getPreferences(Context.MODE_PRIVATE); // get latest preferences, update current MenuItem settings
        SharedPreferences.Editor editor = pref.edit();

        switch (item.getItemId()){
            case R.id.action_currency_list:
                Intent currencyListIntent = new Intent(this, CurrencyListActivity.class);
                startActivity(currencyListIntent);
                return true;
            case R.id.action_refresh_Rates: //start update thread here
                //Starting update thread, which will trigger TOAST in UI thread if successful
                Thread refreshCurrencyThread = new Thread(exchangeRateUpdateRunnable);
                refreshCurrencyThread.start();
                exchangeRateAdapter.notifyDataSetChanged();
                return true;
            case R.id.action_enable_update_service:
                if(item.isChecked()){ // disable update service if already checked
                    item.setChecked(false);
                    scheduler.cancelAll();
                    editor.putBoolean("enableServiceChecked", false);
                    editor.apply();
                    Log.d("OptionsItemSelected", "Cancelled all update Services");
                }
                else {
                    item.setChecked(true);
                    scheduler.schedule(jobInfo);
                    editor.putBoolean("enableServiceChecked", true);
                    editor.apply();
                    Log.d("OptionsItemSelected", "Started update Services");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Button to calculate was clicked
     */
    public void onCalculateClicked(View view){
        double input = 0.0;
        try {
            input = Double.parseDouble(inputEditText.getText().toString());
        }
        catch (Exception e) {
            Toast.makeText(this, "ERROR: Invalid Input", Toast.LENGTH_SHORT).show();
        }

        Log.i("ITEM-SELECTION", "Converting selected item from spinner");
        ExchangeRate from_exchangeRate = (ExchangeRate)fromSpinner.getSelectedItem();
        ExchangeRate to_exchangeRate = (ExchangeRate)toSpinner.getSelectedItem();

        Log.i("SELECTION-Conversion", "From: " + from_exchangeRate.getCurrencyName() + "\t" +
                "To: " + to_exchangeRate.getCurrencyName());

        //double converted = input / from_exchangeRate.getRateForOneEuro() * to_exchangeRate.getRateForOneEuro();
        double converted = exchangeRateDatabase.convert(input, from_exchangeRate.getCurrencyName(), to_exchangeRate.getCurrencyName());

        String result =  String.format("%.2f", converted);
        //String result = String.valueOf(exchangeDatabase.convert(input, from_exchangeRate.getCurrencyName(), to_exchangeRate.getCurrencyName()));
        resultTextView.setText(result);
        setShareText(String.format("%f %s in %s = %f", input, from_exchangeRate.getCurrencyName(), to_exchangeRate.getCurrencyName(), converted));
    }


    @Override
    protected void onResume() {
        super.onResume();

        pref = getPreferences(Context.MODE_PRIVATE);

        // Save values in Preferences
        String input = pref.getString("input_Text", "");
        int fromSelection = pref.getInt("fromSelection", 0);
        int toSelection = pref.getInt("toSelection", 1);

        // manually reset application state
        inputEditText.setText(input);
        fromSpinner.setSelection(fromSelection);
        toSpinner.setSelection(toSelection);


        boolean status = exchangeRateDatabase.loadExchangeRates(this);
        exchangeRateAdapter.notifyDataSetChanged();
        Toast.makeText(this, "Restoring Preferences - " + (status ? "successful" : "failed")
                + "\nInputValue: " + input
                + "\nFrom: " + exchangeRateAdapter.getItem(fromSelection).getCurrencyName()
                + " to: " + exchangeRateAdapter.getItem(toSelection).getCurrencyName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // [Exercise 8 - 5.1]
        // Generate persistent Application Preferences
        pref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        // Collect all data that has to be saved.
        String input = inputEditText.getText().toString();
        int fromSelection = fromSpinner.getSelectedItemPosition();
        int toSelection = toSpinner.getSelectedItemPosition();

        // Save values in Preferences
        editor.putString("input_Text", input);
        editor.putInt("fromSelection", fromSelection);
        editor.putInt("toSelection", toSelection);

        // Save all settings
        editor.apply();

        boolean status = exchangeRateDatabase.saveExchangeRates(this);
        Toast.makeText(this, "Saved exchange rates - " + (status ? "successful" : "failed"), Toast.LENGTH_SHORT).show();

    }
}
