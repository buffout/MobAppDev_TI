package currencyconverter.moad.jh.currencyconverter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class CurrencyListActivity extends AppCompatActivity {

    private ExchangeRateDatabase exchangeDatabase;
    private ExchangeRateAdapter currencyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_list);

        this.exchangeDatabase = new ExchangeRateDatabase();
        this.currencyAdapter = new ExchangeRateAdapter(exchangeDatabase);

        ListView listview = (ListView)findViewById(R.id.activity_currency_list);
        listview.setAdapter(currencyAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l){

                ExchangeRate selectedItem = currencyAdapter.getItem(i);

                Intent intent = new Intent(CurrencyListActivity.this, EditCurrencyActivity.class);
                intent.putExtra("currencyName", selectedItem.getCurrencyName());
                intent.putExtra("rateForOneEuro", selectedItem.getRateForOneEuro());
                //startActivity(intent);
                Log.i("CurrencyListActivity", "Start EditCurrencyActivity to update ExchangeRate for " + selectedItem.getCurrencyName() + " - " + selectedItem.getRateForOneEuro());

                startActivityForResult(intent, i); //Requestcode: i = position in list

                // Option 2: Start Map app to search for capital city name
                //Log.i("ColorListActivity", "Searching map app for: " + capital);
                //Uri geoLocation = Uri.parse("geo:0,0?q="+capital);
                //Intent intent = new Intent(Intent.ACTION_VIEW, geoLocation);
                //if (intent.resolveActivity(getPackageManager()) != null) {
                //    startActivity(intent);
                //}
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null){
            Log.i("CurrencyListActivity", "Data of intent is null (backbutton pressed?)");
            return;
        }

        double newExchangerate = data.getDoubleExtra("rateForOneEuro", 0.0);
        ExchangeRate exchangeRate = this.currencyAdapter.getItem(requestCode);
        Log.i("CurrencyListActivity", "update ExchangeRate for " + exchangeRate.getCurrencyName() + " - " + newExchangerate);
        this.exchangeDatabase.setExchangerate(exchangeRate.getCurrencyName(), newExchangerate);
        this.currencyAdapter.notifyDataSetChanged();
    }
}
