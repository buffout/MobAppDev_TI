package quiz.moad.jh.moadquiz;

import android.util.Log;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by j on 20.10.16.
 */

public class QuestionRepository {
    private ArrayList<Question> questions;

    public QuestionRepository(){
        this.questions = new ArrayList<>();
    }


    public void initQuestions(){
        /*
        this.questions.add(0, new Question("Current OS, on which this APP ist running on?",
                "Windows",
                "Android",
                "iOS",
                "OSX", 1));
        this.questions.add(0, new Question("Question2?",
                "A",
                "B",
                "C",
                "D", 3));
        */
        String rawQuestions = "-----------------------------------------------------------------------------\n" +
                "  #0001 Which decease devastated livestock across the UK during 2001?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Hand-and-foot\n" +
                " *Foot-in-mouth\n" +
                " *Hand-to-mouth\n" +
                " *Foot-and-mouth\n" +
                "\n" +
                "Answer: Foot-and-mouth\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0002 Which of these kills its victims by constriction?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Andalucia\n" +
                " *Anaconda\n" +
                " *Andypandy\n" +
                " *Annerobinson\n" +
                "\n" +
                "Answer: Anaconda\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0003 Which of these might be used in underwater naval operations?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Frogmen\n" +
                " *Newtmen\n" +
                " *Toadmen\n" +
                " *Tadpolemen\n" +
                "\n" +
                "Answer: Frogmen\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0004 In the UK, VAT stands for value-added ...?\n" +
                "----------------------------------------------------------------------------\n" +
                "- *Transaction\n" +
                " *Total\n" +
                " *Tax\n" +
                " *Trauma\n" +
                "\n" +
                "Answer: Tax\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0005 What are you said to do to a habit when you break it?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Throw it\n" +
                " *Punch it\n" +
                " *Kick it\n" +
                " *Eat it\n" +
                "\n" +
                "Answer: Kick it\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0006 Where do you proverbially wear your heart, if you show your true\n" +
                "        feelings?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *On your collar\n" +
                " *On your lapel\n" +
                " *On your cuff\n" +
                " *On your sleeve\n" +
                "\n" +
                "Answer: On your sleeve\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0007 What might an electrician lay?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Tables\n" +
                " *Gables\n" +
                " *Cables\n" +
                " *Stables\n" +
                "\n" +
                "Answer: Cables\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0008 What would a 'tattie picker' harvest?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Raspberries\n" +
                " *Corn\n" +
                " *Potatoes\n" +
                " *Apples\n" +
                "\n" +
                "Answer: Potatoes\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0009 Which of these means adequate space for moving in?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Elbow room\n" +
                " *Foot rest\n" +
                " *Ear hole\n" +
                " *Knee lounge\n" +
                "\n" +
                "Answer: Elbow room\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0010 How is a play on words commonly described?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Pan\n" +
                " *Pin\n" +
                " *Pen\n" +
                " *Pun\n" +
                "\n" +
                "Answer: Pun\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0011 Which colour is used as a term to describe an illegal market in\n" +
                "        rare goods?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Blue\n" +
                " *Red\n" +
                " *Black\n" +
                " *White\n" +
                "\n" +
                "Answer: Black\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0012 Which character was first played by Arnold Schwarzenegger in a 1984\n" +
                "        film?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *The Demonstrator\n" +
                " *The Instigator\n" +
                " *The Investigator\n" +
                " *The Terminator\n" +
                "\n" +
                "Answer: The Terminator\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0013 Which of these would a film actor like to receive?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Oliver\n" +
                " *Oscar\n" +
                " *Oliphant\n" +
                " *Osbert\n" +
                "\n" +
                "Answer: Oscar\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0014 In which country would you expect to be greeted with the word\n" +
                "        'bonjour'?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Italy\n" +
                " *France\n" +
                " *Spain\n" +
                " *Wales\n" +
                "\n" +
                "Answer: France\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0015 What name is given to the person who traditionally attends the\n" +
                "        groom on his wedding day?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Best man\n" +
                " *Top man\n" +
                " *Old man\n" +
                " *Poor man\n" +
                "\n" +
                "Answer: Best man\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0016 Which word follows 'North' and 'South' to give the names of two\n" +
                "        continents?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Africa\n" +
                " *America\n" +
                " *Asia\n" +
                " *Australia\n" +
                "\n" +
                "Answer: America\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0017 Which country is not an island?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Madagascar\n" +
                " *Cuba\n" +
                " *Germany\n" +
                " *Jamaica\n" +
                "\n" +
                "Answer: Germany\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0018 Which is not the name of an English county?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Lancashire\n" +
                " *Leicestershire\n" +
                " *Liverpoolshire\n" +
                " *Lincolnshire\n" +
                "\n" +
                "Answer: Liverpoolshire\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0019 Which of these is a fashionable district of London?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Bulgaria\n" +
                " *Belgravia\n" +
                " *Belgrade\n" +
                " *Belgium\n" +
                "\n" +
                "Answer: Belgravia\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0020 What name is given to a playing card with a single symbol on it?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Whizz\n" +
                " *Hotshot\n" +
                " *Ace\n" +
                " *Star\n" +
                "\n" +
                "Answer: Ace\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0021 What would you normally do with a beret?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Eat it\n" +
                " *Play it\n" +
                " *Sit on it\n" +
                " *Wear it\n" +
                "\n" +
                "Answer: Wear it\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0022 Which of these is a tool for shaping and smoothing wood?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Train\n" +
                " *Plane\n" +
                " *Car\n" +
                " *Bike\n" +
                "\n" +
                "Answer: Plane\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0023 What do the Americans call what we call sweets?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Randy\n" +
                " *Dandy\n" +
                " *Sandy\n" +
                " *Candy\n" +
                "\n" +
                "Answer: Candy\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0024 Which of these is a spicy, Cajun chicken or seafood dish?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Gumbo\n" +
                " *Dumbo\n" +
                " *Bimbo\n" +
                " *Rambo\n" +
                "\n" +
                "Answer: Gumbo\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0025 What would you expect to see at the London Aquarium?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Flowers\n" +
                " *Trees\n" +
                " *Steam rollers\n" +
                " *Fish\n" +
                "\n" +
                "Answer: Fish\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0026 People who are in a similar unfavourable situation are said to be\n" +
                "        'all in the same ...'?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Car\n" +
                " *Plane\n" +
                " *Boat\n" +
                " *Tube\n" +
                "\n" +
                "Answer: Boat\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0027 According to the old adage, how many lives does a cat have?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Five\n" +
                " *Seven\n" +
                " *Nine\n" +
                " *Ten\n" +
                "\n" +
                "Answer: Nine\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0028 Which of these is a keyboard instrument?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Harpsichord\n" +
                " *Ripcord\n" +
                " *Pyjama cord\n" +
                " *Sashcord\n" +
                "\n" +
                "Answer: Harpsichord\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0029 The former punk singer John Lydon was known on stage as Johnny ...?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Horrid\n" +
                " *Rotten\n" +
                " *Nasty\n" +
                " *Hateful\n" +
                "\n" +
                "Answer: Rotten\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0030 The name of which plant sounds like a greeting to 'Coronation\n" +
                "        Street's' Mrs Duckworth?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Hi mabel\n" +
                " *G'day june\n" +
                " *Wotcha di\n" +
                " *Aloe vera\n" +
                "\n" +
                "Answer: Aloe vera\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0031 The Nativity is the story of whose birth?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Jesus Christ\n" +
                " *Elvis Presley\n" +
                " *Winston Churchill\n" +
                " *John F Kennedy\n" +
                "\n" +
                "Answer: Jesus Christ\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0032 How many moons orbit the Earth?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *One\n" +
                " *Two\n" +
                " *Three\n" +
                " *Four\n" +
                "\n" +
                "Answer: One\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0033 By what abbreviation is a compact disc commonly known?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *CD\n" +
                " *COD\n" +
                " *CDIS\n" +
                " *COMPD\n" +
                "\n" +
                "Answer: CD\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0034 Which country shares a land border with the UK?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Portugal\n" +
                " *Libya\n" +
                " *Vietnam\n" +
                " *Ireland\n" +
                "\n" +
                "Answer: Ireland\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0035 The star sign Aquarius is also known as what?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *The Water-carrier\n" +
                " *The Food-carrier\n" +
                " *The Hod-carrier\n" +
                " *The Bag-carrier\n" +
                "\n" +
                "Answer: The Water-carrier\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0036 Which of these would be of most use if you wanted to play poker?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Bat & ball\n" +
                " *Pack of cards\n" +
                " *Swimming trunks\n" +
                " *Horse\n" +
                "\n" +
                "Answer: Pack of cards\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0037 What are said to be 'down' when things are not going well?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Egg\n" +
                " *Bacon\n" +
                " *Chips\n" +
                " *Beans\n" +
                "\n" +
                "Answer: Chips\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0038 What name is given to a mound or ridge of windblown sand?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Drone\n" +
                " *Dude\n" +
                " *Dime\n" +
                " *Dune\n" +
                "\n" +
                "Answer: Dune\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0039 Which part of the human body encases many of the vital organs such\n" +
                "        as the heart, lungs and liver?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Head\n" +
                " *Legs\n" +
                " *Arms\n" +
                " *Torso\n" +
                "\n" +
                "Answer: Torso\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0040 Which of these is a weight category in professional boxing?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Welterweight\n" +
                " *Swelterweight\n" +
                " *Slaughterweight\n" +
                " *Daughterweight\n" +
                "\n" +
                "Answer: Welterweight\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0041 Who has the authority to change a ball during a football match?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Sky Sports\n" +
                " *The home team\n" +
                " *Alex Ferguson\n" +
                " *The referee\n" +
                "\n" +
                "Answer: The referee\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0042 If you are moving downhill over snow with a long runner attached to\n" +
                "        each foot, what are you doing?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Swimming\n" +
                " *Skiing\n" +
                " *Skateboarding\n" +
                " *Showjumping\n" +
                "\n" +
                "Answer: Skiing\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0043 What sort of animal is Jess, who accompanies the TV character\n" +
                "        Postman Pat?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Dog\n" +
                " *Horse\n" +
                " *Cat\n" +
                " *Mouse\n" +
                "\n" +
                "Answer: Cat\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0044 A particular hazard for motorists is black ...?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Pudding\n" +
                " *Ice\n" +
                " *Holes\n" +
                " *Mascara\n" +
                "\n" +
                "Answer: Ice\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0045 Which 'Dallas' spin-off saw Gary Ewing arrive in a small\n" +
                "        Californian town?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Knots Patio\n" +
                " *Knots Garage\n" +
                " *Knots Porch\n" +
                " *Knots Landing\n" +
                "\n" +
                "Answer: Knots Landing\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0046 Which is a US-made soap opera?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Johnson's Mere\n" +
                " *Thompson's Pond\n" +
                " *Dawson's Creek\n" +
                " *Madison's Lake\n" +
                "\n" +
                "Answer: Dawson's Creek\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0047 Which of these is a discipline in both men's and women's\n" +
                "        gymnastics?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Floor exercises\n" +
                " *Wall exercises\n" +
                " *Ceiling exercises\n" +
                " *Roof exercises\n" +
                "\n" +
                "Answer: Floor exercises\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0048 Which of the following do bowlers aim to hit during a game of\n" +
                "        cricket?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Posts\n" +
                " *Hoop\n" +
                " *Net\n" +
                " *Stumps\n" +
                "\n" +
                "Answer: Stumps\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0049 Which of these applies to the shape of a soccer ball?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Conical\n" +
                " *Cylindrical\n" +
                " *Spherical\n" +
                " *Oval\n" +
                "\n" +
                "Answer: Spherical\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0050 Which novel by Charles Dickens was made into a film in 2002?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Danny Dimeby\n" +
                " *Nicholas Nickleby\n" +
                " *Quentin Quarterby\n" +
                " *Donald Dollarby\n" +
                "\n" +
                "Answer: Nicholas Nickleby\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0051 A 'cuppa' is an informal term for what?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Policeman\n" +
                " *Cup of tea\n" +
                " *2p coin\n" +
                " *Smoked herring\n" +
                "\n" +
                "Answer: Cup of tea\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0052 What is the meaning of the colloquial expression 'in the bag'?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Almost certain\n" +
                " *Newly bought\n" +
                " *Freshly cooked\n" +
                " *Recently stolen\n" +
                "\n" +
                "Answer: Almost certain\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0053 Which activity would you most associate with a mole?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Burrowing\n" +
                " *Climbing\n" +
                " *Swimming\n" +
                " *Flying\n" +
                "\n" +
                "Answer: Burrowing\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0054 Which is not a type of antelope?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Gorilla\n" +
                " *Gerenuk\n" +
                " *Gemsbok\n" +
                " *Gnu\n" +
                "\n" +
                "Answer: Gorilla\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0055 Which is an alternative name for members of the Society of Friends?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Kellogg's\n" +
                " *Special Ks\n" +
                " *Quakers\n" +
                " *Jordans\n" +
                "\n" +
                "Answer: Quakers\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0056 Which is another name for a short melodious tune?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Oxygen\n" +
                " *Air\n" +
                " *Nitrogen\n" +
                " *Stratosphere\n" +
                "\n" +
                "Answer: Air\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0057 Which of these is a large woodwind instrument?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Buffoon\n" +
                " *Pantaloon\n" +
                " *Bassoon\n" +
                " *Macaroon\n" +
                "\n" +
                "Answer: Bassoon\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0058 In the nursery rhyme, who met a pieman?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Easy Eric\n" +
                " *Plain Peter\n" +
                " *Simple Simon\n" +
                " *No Frills Fred\n" +
                "\n" +
                "Answer: Simple Simon\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0059 Which of these is a material often used to make floor mats?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Rush\n" +
                " *Dash\n" +
                " *Hustle\n" +
                " *Bustle\n" +
                "\n" +
                "Answer: Rush\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0060 In which of the following might food be stored?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Larder\n" +
                " *Shed\n" +
                " *Greenhouse\n" +
                " *Garage\n" +
                "\n" +
                "Answer: Larder\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0061 What is rioja a type of?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Bread\n" +
                " *Vegetable\n" +
                " *Wine\n" +
                " *Nut\n" +
                "\n" +
                "Answer: Wine\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0062 Germania was the Roman name for which modern-day European country?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *France\n" +
                " *Austria\n" +
                " *Germany\n" +
                " *Spain\n" +
                "\n" +
                "Answer: Germany\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0063 What was the UK's top paying attraction of 2002?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *London Nose\n" +
                " *London Mouth\n" +
                " *London Eye\n" +
                " *London Ear\n" +
                "\n" +
                "Answer: London Eye\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0064 Which of these is a Scottish district council?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Inverchristopher\n" +
                " *Inverclyde\n" +
                " *Inverclint\n" +
                " *Invercharles\n" +
                "\n" +
                "Answer: Inverclyde\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0065 In which town are the administrative headquarters of Kent?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Maidstone\n" +
                " *Lasspebble\n" +
                " *Girlrock\n" +
                " *Missgravel\n" +
                "\n" +
                "Answer: Maidstone\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0066 Which of these geographical features is a mountain?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Kilimanjaro\n" +
                " *Danube\n" +
                " *Amazon\n" +
                " *Nile\n" +
                "\n" +
                "Answer: Kilimanjaro\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0067 Which word goes before 'States of America' to give the name of a\n" +
                "        country?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Joined\n" +
                " *Agreed\n" +
                " *United\n" +
                " *Harmonious\n" +
                "\n" +
                "Answer: United\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0068 Which was a famous group of high-kicking stage dancers?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Checkout Girls\n" +
                " *Cashpoint Girls\n" +
                " *Tiller Girls\n" +
                " *Turnstile Girls\n" +
                "\n" +
                "Answer: Tiller Girls\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0069 Which of these is a person who performs tricks that deceive the\n" +
                "        eye?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Illustrator\n" +
                " *Illuminator\n" +
                " *Illiterate\n" +
                " *Illusionist\n" +
                "\n" +
                "Answer: Illusionist\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0070 Which of the following words for a coat also describes the paper\n" +
                "        cover of a book?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Cagoule\n" +
                " *Mackintosh\n" +
                " *Jacket\n" +
                " *Parka\n" +
                "\n" +
                "Answer: Jacket\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0071 What is hung over a horse's head for feeding?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Nosebag\n" +
                " *Nosegay\n" +
                " *Nosedive\n" +
                " *Nosejob\n" +
                "\n" +
                "Answer: Nosebag\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0072 Something mediocre can be described as 'no great ...'?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Shakes\n" +
                " *Quivers\n" +
                " *Wobbles\n" +
                " *Trembles\n" +
                "\n" +
                "Answer: Shakes\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0073 What do you proverbially let down when behaving without reserve?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Shoulders\n" +
                " *Elbows\n" +
                " *Knees\n" +
                " *Hair\n" +
                "\n" +
                "Answer: Hair\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0074 Which is not a species of seal?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Elephant\n" +
                " *Leopard\n" +
                " *Three-legged\n" +
                " *Grey\n" +
                "\n" +
                "Answer: Three-legged\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0075 Which keyboard player was awarded the OBE in the 2003 Birthday\n" +
                "        Honours list?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Cools Belgium\n" +
                " *Drools Luxembourg\n" +
                " *Fools France\n" +
                " *Jools Holland\n" +
                "\n" +
                "Answer: Jools Holland\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0076 Which show took place between 6th-9th March 2004 at the NEC,\n" +
                "        Birmingham?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Toffs\n" +
                " *Cuffs\n" +
                " *Crufts\n" +
                " *Lifts\n" +
                "\n" +
                "Answer: Crufts\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0077 Harrison Ford announced in August 2003, that he would make a fourth\n" +
                "        film playing which role?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Nevada Morgan\n" +
                " *Wyoming Williams\n" +
                " *Indiana Jones\n" +
                " *Montana Evans\n" +
                "\n" +
                "Answer: Indiana Jones\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0078 August 16th 2003 was the 40th anniversary of which 'Great' robbery?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Hovercraft\n" +
                " *Pushchair\n" +
                " *Balloon\n" +
                " *Train\n" +
                "\n" +
                "Answer: Train\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0079 Which member of the Royal Family celebrated his 19th birthday in\n" +
                "        September 2003?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Prince Edward\n" +
                " *Prince Andrew\n" +
                " *Prince Harry\n" +
                " *Prince Charles\n" +
                "\n" +
                "Answer: Prince Harry\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0080 With which football club was David Beckham's name not linked in\n" +
                "        2003?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Barcelona\n" +
                " *Scunthorpe United\n" +
                " *AC Milan\n" +
                " *Real Madrid\n" +
                "\n" +
                "Answer: Scunthorpe United\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0081 Whose return to 'EastEnders' in 2004 sparked a 560 megawatt power\n" +
                "        surge on the national grid?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Filthy Fred\n" +
                " *Smutty Sam\n" +
                " *Grubby Gordon\n" +
                " *Dirty Den\n" +
                "\n" +
                "Answer: Dirty Den\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0082 Which patron saint's day falls on 23rd April?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *St Harry\n" +
                " *St Albert\n" +
                " *St George\n" +
                " *St William\n" +
                "\n" +
                "Answer: St George\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0083 What is celebrated in Wales on 1st March?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Gareth Edwards Day\n" +
                " *St David's Day\n" +
                " *Tom Jones Day\n" +
                " *Richard Burton Day\n" +
                "\n" +
                "Answer: St David's Day\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0084 Which adjective applied to Friday 9th April in 2004?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Good\n" +
                " *Bad\n" +
                " *Ugly\n" +
                " *Indifferent\n" +
                "\n" +
                "Answer: Good\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0085 Which of these is a popular form of music?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *County & Eastern\n" +
                " *Kingdom & Northern\n" +
                " *Land & Southern\n" +
                " *Country & Western\n" +
                "\n" +
                "Answer: Country & Western\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0086 Which of these is a slang term for a mean person?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Cheapskate\n" +
                " *Cheapshark\n" +
                " *Cheapmackerel\n" +
                " *Cheaphaddock\n" +
                "\n" +
                "Answer: Cheapskate\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0087 Which of these is a common term for a programme of physical\n" +
                "        exercises?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Stay able\n" +
                " *Remain trim\n" +
                " *Continue competent\n" +
                " *Keep fit\n" +
                "\n" +
                "Answer: Keep fit\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0088 What is the usual name for an establishment containing a lot of\n" +
                "        fruit machines?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Amusement arcade\n" +
                " *Happy house\n" +
                " *Fun folly\n" +
                " *Hilarity hall\n" +
                "\n" +
                "Answer: Amusement arcade\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0089 Which of these means displaying advertising posters in an\n" +
                "        unauthorised place?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Ant-posting\n" +
                " *Bee-posting\n" +
                " *Fly-posting\n" +
                " *Wasp-posting\n" +
                "\n" +
                "Answer: Fly-posting\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0090 On which of these might you win a large amount of money?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *National Flattery\n" +
                " *National Lottery\n" +
                " *National Battery\n" +
                " *National Pottery\n" +
                "\n" +
                "Answer: National Lottery\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0091 What kind of envelope has a transparent section through which the\n" +
                "        address can be seen?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Fanlight\n" +
                " *Door\n" +
                " *Sunroof\n" +
                " *Window\n" +
                "\n" +
                "Answer: Window\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0092 Which of these is a type of beer?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Acid\n" +
                " *Bitter\n" +
                " *Tart\n" +
                " *Sour\n" +
                "\n" +
                "Answer: Bitter\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0093 What is a woman said to do with her eyelashes when she is being\n" +
                "        flirtatious?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Litter\n" +
                " *Platter\n" +
                " *Flutter\n" +
                " *Rotter\n" +
                "\n" +
                "Answer: Flutter\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0094 Which of these is an ice cream dessert?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Sundae\n" +
                " *Mondae\n" +
                " *Tuesdae\n" +
                " *Wednesdae\n" +
                "\n" +
                "Answer: Sundae\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0095 Which term means replacing a telephone receiver at the end of a\n" +
                "        call?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Jump up\n" +
                " *Hang up\n" +
                " *Toss up\n" +
                " *Play up\n" +
                "\n" +
                "Answer: Hang up\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0096 Which of these is a popular garden flower?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Busnation\n" +
                " *Carnation\n" +
                " *Trainnation\n" +
                " *Planenation\n" +
                "\n" +
                "Answer: Carnation\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0097 Which of these describes something which causes stress or anxiety?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Knee-racking\n" +
                " *Nerve-racking\n" +
                " *Nodule-racking\n" +
                " *Nose-racking\n" +
                "\n" +
                "Answer: Nerve-racking\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0098 Which of these is a device often used in a séance?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Ironing board\n" +
                " *Cheeseboard\n" +
                " *Circuit board\n" +
                " *Ouija board\n" +
                "\n" +
                "Answer: Ouija board\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0099 Which of these is a child's playground game in which participants\n" +
                "        slap each other's hands?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Pat-a-bap\n" +
                " *Pat-a-roll\n" +
                " *Pat-a-biscuit\n" +
                " *Pat-a-cake\n" +
                "\n" +
                "Answer: Pat-a-cake\n" +
                "\n" +
                "-----------------------------------------------------------------------------\n" +
                "  #0100 Which of these is a term for a typically average man?\n" +
                "-----------------------------------------------------------------------------\n" +
                " *Joe Cole\n" +
                " *Joe Bloggs\n" +
                " *Joe Bugner\n" +
                " *Joe Strummer\n" +
                "\n" +
                "Answer: Joe Bloggs";

        // [QUESTIONS] original Pattern: \#[^\?]+\?
        Matcher m_questions = Pattern.compile("\\#[^\\?]+\\?").matcher(rawQuestions);

        // [ANSWERS] original Pattern: (?<=\*)(.*)
        Matcher m_answers = Pattern.compile("(?<=\\*)(.*)").matcher(rawQuestions);

        // [CORRECT_ANS] original Patter: (?<=: )(.*)
        Matcher m_correctAns = Pattern.compile("(?<=: )(.*)").matcher(rawQuestions);

        while(m_questions.find()){
            Log.i("new Question:", m_questions.group(0));
            String questionText = m_questions.group(0);

            m_correctAns.find();
            String correctAns = m_correctAns.group(0);
            Log.i("new Correct Answer:", correctAns);
            int correctIdx = 3;

            String[] answers = new String[4];
            for (int i = 0; i < 4; i++){
                m_answers.find();
                answers[i] = m_answers.group(0);
                Log.i("new Answer:", answers[i]);
                if (correctAns.equals(answers[i])){
                    Log.i("Corrdect Idx found", correctIdx + answers[i]);
                    correctIdx = i;
                }
            }
            this.questions.add(new Question(questionText, answers[0], answers[1], answers[2], answers[3], correctIdx));
        }
    }

    public Question getRandomQuestion(){
        return this.questions.get((int) (Math.random() * this.questions.size()));
    }
}
