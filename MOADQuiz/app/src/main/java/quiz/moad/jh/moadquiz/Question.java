package quiz.moad.jh.moadquiz;

/**
 * Created by j on 20.10.16.
 */

public class Question {
    private String question;
    private String[] answers;
    private int correctAnswer;


    public Question(String question, String answer1, String answer2, String answer3, String answer4, int correctAnswer){
        this.question =  question;
        this.answers = new String[] {answer1, answer2, answer3, answer4};
        this.correctAnswer = correctAnswer;
    }
git config --global user.email "mega.jh@gmail.com"


    public String getQuestion(){
        return this.question;
    }

    public String getAnswer(int answerID){
        return this.answers[answerID];
    }

    public int getCorrectAnswer(){
        return this.correctAnswer;
    }
}
