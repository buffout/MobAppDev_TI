package quiz.moad.jh.moadquiz;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private QuestionRepository questRepo;
    private Question currQuest;

    private TextView questionText;

    private Button btnAnswer1;
    private Button btnAnswer2;
    private Button btnAnswer3;
    private Button btnAnswer4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.questRepo = new QuestionRepository();
        this.questRepo.initQuestions();

        this.questionText = (TextView) findViewById(R.id.questionTextView);
        this.btnAnswer1 = (Button) findViewById(R.id.btnAnswer1);
        this.btnAnswer2 = (Button) findViewById(R.id.btnAnswer2);
        this.btnAnswer3 = (Button) findViewById(R.id.btnAnswer3);
        this.btnAnswer4 = (Button) findViewById(R.id.btnAnswer4);
        updateQuestion();


    }

    private void updateQuestion(){
        this.btnAnswer1.setBackgroundResource(android.R.drawable.btn_default);
        this.btnAnswer2.setBackgroundResource(android.R.drawable.btn_default);
        this.btnAnswer3.setBackgroundResource(android.R.drawable.btn_default);
        this.btnAnswer4.setBackgroundResource(android.R.drawable.btn_default);

        this.btnAnswer1.setEnabled(true);
        this.btnAnswer2.setEnabled(true);
        this.btnAnswer3.setEnabled(true);
        this.btnAnswer4.setEnabled(true);


        this.currQuest = this.questRepo.getRandomQuestion();
        this.questionText.setText(this.currQuest.getQuestion());
        this.btnAnswer1.setText(this.currQuest.getAnswer(0));
        this.btnAnswer2.setText(this.currQuest.getAnswer(1));
        this.btnAnswer3.setText(this.currQuest.getAnswer(2));
        this.btnAnswer4.setText(this.currQuest.getAnswer(3));

    }

    public void answerButtonClicked(View view){
        Button btnanswer = (Button)findViewById(view.getId());                ;
        String answer = (String) btnanswer.getText();

        boolean correct = this.currQuest.getAnswer(this.currQuest.getCorrectAnswer()) == answer;
        if (correct){
            btnanswer.setBackgroundColor(Color.GREEN);
            updateQuestion();
        } else {
            btnanswer.setBackgroundColor(Color.RED);
            btnanswer.setEnabled(false);
        }
    }
}
