package listviewdemo.moad.jh.listvewdemo;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by j on 10.11.16.
 */
public class ColorDetailsActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_details);

        String colorName = (String)getIntent().getSerializableExtra("colorName");
        int colorCode = (int)getIntent().getSerializableExtra("colorCode");
        Log.i("ColorDetailActivity", "ColorName:" + colorName + "\tColorCode: " + colorCode);

        TextView textView = (TextView)findViewById(R.id.color_detail); //Convert EditText to TextView
        textView.setText(colorName);
        textView.setBackgroundColor(colorCode);

        final EditText editText = (EditText)findViewById(R.id.color_detail);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE){
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("colorName", editText.getText().toString());
                    setResult(RESULT_OK, returnIntent); //send Result data after closing current Activity
                    Log.i("EditText", "new Color name = " + editText.getText().toString());
                    finish(); // Close on Return pressed EditText
                    return true;
                }
                return false;
            }
        });

    }
}
