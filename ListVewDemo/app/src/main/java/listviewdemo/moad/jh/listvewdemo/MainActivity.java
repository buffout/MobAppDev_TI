package listviewdemo.moad.jh.listvewdemo;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ColorListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ColorEntry[]colorEntries = {
                new ColorEntry("Red", Color.RED),
                new ColorEntry("Green", Color.GREEN),
                new ColorEntry("Blue", Color.BLUE)
        };

        this.adapter = new ColorListAdapter(colorEntries);

        ListView listview = (ListView)findViewById(R.id.List_view);
        listview.setAdapter(adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l){
                ColorEntry entry = (ColorEntry)adapter.getItem(i);

                Intent intent = new Intent(MainActivity.this, ColorDetailsActivity.class);
                intent.putExtra("colorName", entry.colorName);
                intent.putExtra("colorCode", entry.colorCode);
                Log.i("MainActivity", "ColorName:" + entry.colorName + " ColorCode: " + entry.colorCode);

                //startActivity(intent);
                startActivityForResult(intent, i); //Requestcode: i = position in list
            }
        });
        /* // Old Demo, see MOAD_3-Apps_GUI.pdf
        String[] tage = {"Montag", "Dienstag", "Mittwoch", "Donnerstag",
                "Freitag", "Samstag", "Sonntag"};

        ListView listView = (ListView)findViewById(R.id.entry_text_view);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.list_item_layout,
                R.id.entry_text_view,
                tage
        );

        listView.setAdapter(adapter);
        */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String newName = data.getStringExtra("colorName");
        ColorEntry entry = (ColorEntry)adapter.getItem(requestCode);
        entry.colorName = newName;
        adapter.notifyDataSetChanged();
    }
}
