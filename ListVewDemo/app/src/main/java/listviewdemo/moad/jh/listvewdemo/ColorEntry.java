package listviewdemo.moad.jh.listvewdemo;

import android.graphics.Color;

/**
 * Created by j on 27.10.16.
 */

public class ColorEntry {
    public String colorName;
    public int colorCode;
    public ColorEntry(String colorName, int colorCode){
        this.colorName = colorName;
        this.colorCode = colorCode;
    }
}
