package listviewdemo.moad.jh.listvewdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by j on 27.10.16.
 */

public class ColorListAdapter extends BaseAdapter {
    private ColorEntry[] entries;

    public ColorListAdapter(ColorEntry[] entries){
        this.entries = entries;
    }

    @Override
    public int getCount() {
        return entries.length;
    }

    @Override
    public Object getItem(int i) {
        return this.entries[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        ColorEntry entry = entries[i];

        if(view == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_layout, null, false);
        }

        View colorBox = view.findViewById(R.id.List_ColorBox);
        colorBox.setBackgroundColor(entry.colorCode);

        TextView textView = (TextView)view.findViewById(R.id.List_TextElement);
        textView.setText(entry.colorName);
        return view;
    }
}
