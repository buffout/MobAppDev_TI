package madmusicplayer.moad.jh.madmusicplayer;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by j on 20.01.17.
 */

public class MusicLibrary {

    private Cursor cur;

    private ArrayList<Song> songs;

    public MusicLibrary(Context context){
        songs = new ArrayList<>();

        int permissionCheck = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_CALENDAR);

        Log.d("Permission", "Permission status" + permissionCheck);

        if (permissionCheck != 0){
            ContentResolver mediaProvider = context.getContentResolver();

            cur = mediaProvider.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,null, null, null, null);

            if (cur.getCount() > 0){
                while(cur.moveToNext()){
                    long id = cur.getLong(cur.getColumnIndex(MediaStore.Audio.Media._ID));
                    String artist = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    String title = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String album = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    Log.i("FoundAudio", "artist: " + artist + "\ttitle: " + title + "\talbum: " + album);

                    songs.add(new Song(id, title, album, artist));
                }
            }

        }
    }

    public Song getRandomSong(){
        int rangomIdx = (int)(Math.random() * songs.size());
        return songs.get(rangomIdx);
    }

    public Song getSong(int i){
        return songs.get(i);
    }

    public int getNumSongs(){
        return songs.size();
    }

}
