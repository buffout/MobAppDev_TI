package madmusicplayer.moad.jh.madmusicplayer;

/**
 * Created by Derpzilla on 20.01.2017.
 */

public class Song {
    long id;
    private String title;
    private String album;
    private String artist;

    public Song(long id, String title, String album, String artist) {
        this.id = id;
        this.title = title;
        this.album = album;
        this.artist = artist;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }
}
