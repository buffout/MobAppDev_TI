package madmusicplayer.moad.jh.madmusicplayer;

import android.Manifest;
import android.content.ContentUris;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener{

    private MediaPlayer mediaPlayer;
    private Button playButton;

    private TextView textViewInterpret;
    private TextView textViewTitle;
    private TextView textViewAlbum;
    private MusicLibrary musicLibrary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // TODO list all songs
        // get file URI from list
        //mediaPlayer = new MediaPlayer().create(this, );

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                int permissionID = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, permissionID);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        musicLibrary = new MusicLibrary(this);

        Song start = (Song) musicLibrary.getSong(0);
        textViewInterpret = (TextView) findViewById(R.id.textViewInterpret);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewAlbum = (TextView) findViewById(R.id.textViewAlbum);


        // 1. Create MediaPlayer
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        // 2. Set fist resource file
        Uri songURI = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, musicLibrary.getSong(0).getId());
        try
        {
            mediaPlayer.setDataSource(this, songURI);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // 3. set onPreparedListener, will be called after preparation, without interruption of the UI thread
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.prepareAsync();

        playButton = (Button) findViewById(R.id.btnPlay);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer){
        playButton.setEnabled(true);
    }

    public void onButtonPlayClicked(View view){
        if (!mediaPlayer.isPlaying()){
            Log.i("MusicPlayer", "Playing...");
            mediaPlayer.start();
        }
        else{
            Log.i("MusicPlayer", "Stopped playing");
            mediaPlayer.pause();
        }
    }

    private void updateMediaText(Song song){
        textViewInterpret.setText(song.getArtist());
        textViewTitle.setText(song.getTitle());
        textViewAlbum.setText(song.getAlbum());
    }

    public void onButtonShuffleClicked(View view){
        // 2. Set fist resource file
        Song randomSong = musicLibrary.getRandomSong();
        updateMediaText(randomSong);

        mediaPlayer.reset();
        Uri songURI = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, randomSong.getId());
        try
        {
            mediaPlayer.setDataSource(this, songURI);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        mediaPlayer.prepareAsync();
    }

}
