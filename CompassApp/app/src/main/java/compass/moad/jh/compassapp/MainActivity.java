package compass.moad.jh.compassapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private SensorManager sensorManager;
    private Sensor sensor;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        this.sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);

        List<Sensor> lightSensor = sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        if(lightSensor.isEmpty()) {finish(); return;}

        this.sensor = lightSensor.get(0);
        Toast.makeText(this, "Registered Compass Sensor", Toast.LENGTH_SHORT);

        imageView = (ImageView) findViewById(R.id.compass_image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings_fast:
                Log.i("UpdateSensor", "FASTEST");
                reloadSensor(SensorManager.SENSOR_DELAY_FASTEST);
                return true;
            case R.id.settings_normal:
                Log.i("UpdateSensor", "NORMAL");
                reloadSensor(SensorManager.SENSOR_DELAY_UI);
                return true;
            case R.id.settings_slow:
                Log.i("UpdateSensor", "SLOWEST");
                reloadSensor(SensorManager.SENSOR_DELAY_NORMAL);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void reloadSensor(int sampleRate){
        this.sensorManager.unregisterListener(this);
        this.sensorManager.registerListener(this, this.sensor, sampleRate);
    }

    // 3.a Registriren
    @Override
    protected void onResume() {
        super.onResume();
        this.sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_GAME);
    }

     // 3.b Abmelden
    @Override
    protected void onPause() {
        super.onPause();
        this.sensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] vals = sensorEvent.values.clone();
        this.imageView.setRotation(-vals[0]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
